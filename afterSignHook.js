const fs = require('fs');
const path = require('path');
const electronNotarize = require('electron-notarize');

require('dotenv').config();

module.exports = async function (params) {
  if (process.platform !== 'darwin') {
    return;
  }
  const appId = 'com.nativedsd.downloader';
  const appPath = path.join(
    params.appOutDir,
    `${params.packager.appInfo.productFilename}.app`
  );
  if (!fs.existsSync(appPath)) {
    throw new Error(`Cannot find application at: ${appPath}`);
  }
  try {
    await electronNotarize.notarize({
      appBundleId: appId,
      appPath,
      appleId: process.env.APPLE_ID,
      appleIdPassword: process.env.APPLE_ID_PASSWORD,
    });
  } catch (error) {
    console.error(error);
  }
  console.log(`Done notarizing ${appId}`);
};
