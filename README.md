## Starting Development

Start the app in the `dev` environment.

```bash
yarn dev
```

## Packaging for Production

To package apps for the local platform:

```bash
yarn package
```
