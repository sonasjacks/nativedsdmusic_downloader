// @flow

export const SET_DOWNLOAD_ALBUMS = 'SET_DOWNLOAD_ALBUMS';
export const SET_DOWNLOAD_ARTWORKS = 'SET_DOWNLOAD_ARTWORKS';
export const SET_DOWNLOAD_COVERS = 'SET_DOWNLOAD_COVERS';

export const ADD_DOWNLOAD_ALBUMS = 'ADD_DOWNLOAD_ALBUMS';
export const ADD_DOWNLOAD_COVER = 'ADD_DOWNLOAD_COVER';
export const ADD_DOWNLOAD_ARTWORK = 'ADD_DOWNLOAD_ARTWORK';

export const REMOVE_DOWNLOAD_COVER = 'REMOVE_DOWNLOAD_COVER';
export const REMOVE_DOWNLOAD_ARTWORK = 'REMOVE_DOWNLOAD_ARTWORK';

export const SET_DOWNLOAD_ERROR = 'SET_DOWNLOAD_ERROR';

export function setDownloadAlbums(albums) {
  return {
    type: SET_DOWNLOAD_ALBUMS,
    payload: albums,
  };
}

export function setDownloadCovers(covers) {
  return {
    type: SET_DOWNLOAD_COVERS,
    payload: covers,
  };
}

export function setDownloadArtworks(artworks) {
  return {
    type: SET_DOWNLOAD_ARTWORKS,
    payload: artworks,
  };
}

export function addDownloadAlbums(albums) {
  return {
    type: ADD_DOWNLOAD_ALBUMS,
    payload: albums,
  };
}

export function addDownloadCover(cover) {
  return {
    type: ADD_DOWNLOAD_COVER,
    payload: cover,
  };
}

export function addDownloadArtwork(artwork) {
  return {
    type: ADD_DOWNLOAD_ARTWORK,
    payload: artwork,
  };
}

export function removeDownloadCover(cover) {
  return {
    type: REMOVE_DOWNLOAD_COVER,
    payload: cover,
  };
}

export function removeDownloadArtwork(artwork) {
  return {
    type: REMOVE_DOWNLOAD_ARTWORK,
    payload: artwork,
  };
}

export function setDownloadError(error) {
  return {
    type: SET_DOWNLOAD_ERROR,
    payload: error,
  };
}
