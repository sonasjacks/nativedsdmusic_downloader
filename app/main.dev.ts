/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `yarn build` or `yarn build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 */
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import path from 'path';
import electron, {
  app,
  BrowserWindow,
  ipcMain,
  dialog,
  Notification,
} from 'electron';
import { autoUpdater } from 'electron-updater';
import fs from 'fs';
import log from 'electron-log';
import sourceMapSupport from 'source-map-support';
import electronDebug from 'electron-debug';
import * as installer from 'electron-devtools-installer';
import MenuBuilder from './menu';
import store from './utils/storage';

let mainWindow: BrowserWindow | null = null;

ipcMain.on('open-file-dialog', (event) => {
  let defaultPath = store.get(
    'downloadLocation',
    `${app.getPath('music')}\\NativeDSD`
  );

  if (!fs.existsSync(defaultPath)) {
    defaultPath = `${app.getPath('music')}\\NativeDSD`;
  }
  dialog
    .showOpenDialog(mainWindow, {
      defaultPath,
      properties: ['openDirectory'],
    })
    .then((result) => {
      if (result.filePaths.length > 0)
        event.sender.send('selected-file', result.filePaths);
      return result.filePaths;
    })
    .catch((error) => {
      console.log(error);
    });
});

ipcMain.on('notify', (event, body) => {
  const notification = new Notification({
    title: app.name,
    body,
    icon: '../resources/icon.png',
  });
  if (store.get('notificationPermission', true)) {
    notification.show();
  }
});

export default class AppUpdater {
  constructor() {
    log.transports.file.level = 'debug';
    autoUpdater.logger = log;
    log.info(autoUpdater.getFeedURL());
    autoUpdater.autoDownload = true;
    autoUpdater.autoInstallOnAppQuit = true;
    autoUpdater.checkForUpdatesAndNotify();
  }
}

autoUpdater.on('update-downloaded', (ev, info) => {
  const choice = electron.dialog.showMessageBox({
    type: 'question',
    buttons: ['Cancel', 'Install'],
    title: 'Update',
    message: 'Update successfully downloaded',
    detail: 'The app will quit and install the update.',
    icon: '../resources/icon.png',
    cancelId: 0,
    defaultId: 1,
  });
  choice
    .then((result) => {
      if (result.response === 1) {
        setImmediate(() => {
          app.removeAllListeners('before-quit');
          app.removeAllListeners('window-all-closed');
          mainWindow?.removeAllListeners('close');
          mainWindow?.close();
          autoUpdater.quitAndInstall();
        });
      }
      return result;
    })
    .catch((error) => {
      console.log(error);
    });
});

if (process.env.NODE_ENV === 'production') {
  sourceMapSupport.install();
}

if (
  process.env.NODE_ENV === 'development' ||
  process.env.DEBUG_PROD === 'true'
) {
  electronDebug();
}

const installExtensions = async () => {
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = ['REACT_DEVELOPER_TOOLS', 'REDUX_DEVTOOLS'];

  return installer
    .default(
      extensions.map((name) => installer[name]),
      forceDownload
    )
    .catch(console.log);
};

const createWindow = async () => {
  if (
    process.env.NODE_ENV === 'development' ||
    process.env.DEBUG_PROD === 'true'
  ) {
    await installExtensions();
  }

  mainWindow = new BrowserWindow({
    show: false,
    width: 1024,
    height: 728,
    minWidth: 800,
    minHeight: 500,
    // icon: path.join(__dirname, '/app.icns'),
    titleBarStyle: 'hidden',
    webPreferences:
      (process.env.NODE_ENV === 'development' ||
        process.env.E2E_BUILD === 'true') &&
      process.env.ERB_SECURE !== 'true'
        ? {
            nodeIntegration: true,
          }
        : {
            preload: path.join(__dirname, 'dist/renderer.prod.js'),
          },
  });

  mainWindow.loadURL(`file://${__dirname}/app.html`);

  mainWindow.once('ready-to-show', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    if (process.env.START_MINIMIZED) {
      mainWindow.minimize();
    } else {
      mainWindow.show();
      mainWindow.focus();
    }
  });

  mainWindow.on('close', (event) => {
    if (!app.quitting && process.platform === 'darwin') {
      event.preventDefault();
      mainWindow.hide();
    }
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  const menuBuilder = new MenuBuilder(mainWindow);
  menuBuilder.buildMenu();

  // Remove this if your app does not use auto updates
  // eslint-disable-next-line
  new AppUpdater();
  store.set('downloadFolder', app.getPath('music'));
  // global.downloadFolder = app.getPath('music');
};

/**
 * Add event listeners...
 */

app.on('window-all-closed', () => {
  // Respect the OSX convention of having the application in memory even
  // after all windows have been closed
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.whenReady().then(createWindow).catch(console.log);

// if (process.env.E2E_BUILD === 'true') {
//   // eslint-disable-next-line promise/catch-or-return
//   app.whenReady().then(createWindow);
// } else {
//   app.on('ready', createWindow);
// }

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow();
  else mainWindow.show();
});

app.on('before-quit', (event) => {
  const choice = electron.dialog.showMessageBox({
    type: 'question',
    buttons: ['Yes', 'No'],
    title: 'Confirm',
    message: 'Are you sure you want to quit?',
    detail:
      'Any active downloads will be aborted. The downloads history list will be cleared.',
    icon: '../resources/icon.png',
  });
  choice
    .then(({ response }) => {
      if (response === 1) {
        event.preventDefault();
      } else app.quitting = true;
      return response;
    })
    .catch((error) => {
      console.log(error);
    });
});
