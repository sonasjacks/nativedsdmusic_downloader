import { AxiosError } from 'axios';
import { createContext } from 'react';

export const ErrorContext = createContext<{
  error: AxiosError | null;
  setError: (error: AxiosError | null) => void;
}>({
  error: null,
  setError: () => undefined,
});

export const ErrorContextConsumer = ErrorContext.Consumer;
export const ErrorContextProvider = ErrorContext.Provider;
