import Store from 'electron-store';

const store = new Store({
  configName: 'user-preferences',
  defaults: {
    windowBounds: {
      width: 1280,
      height: 720,
    },
    account: 'levels',
    node: 0,
  },
});

export default store;
