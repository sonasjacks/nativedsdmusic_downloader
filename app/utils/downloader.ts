import fs from 'fs';
import electron, { ipcRenderer } from 'electron';
import got from 'got';
import {
  setDownloadAlbums,
  setDownloadCovers,
  setDownloadArtworks,
  addDownloadAlbums,
} from '../actions/downloads';
import electronStore from './storage';
import { store } from '../store';

export const getDownloadPath = () => {
  const defaultFolder = `${electronStore.get('downloadFolder')}//NativeDSD`;
  const path = electronStore.get('downloadLocation', defaultFolder);
  if (!fs.existsSync(String(path))) {
    fs.mkdirSync(String(path));
  }
  return path;
};

export const getCurrentDownloadingCount = () => {
  const { downloadAlbums } = store.getState().downloads;
  let count = 0;
  downloadAlbums.forEach((album) => {
    if (album.status === 'progress' || album.status === 'downloading')
      count += 1;
  });
  return count;
};

export const getDownloadAlbums = () => {
  const { downloadAlbums } = store.getState().downloads;
  const albums = [];
  downloadAlbums.forEach((album) => {
    albums.push({ ...album });
  });
  return albums;
};

export const getDownloadCovers = () => {
  const { downloadCovers } = store.getState().downloads;
  const covers = [];
  downloadCovers.forEach((cover) => {
    covers.push({ ...cover });
  });
  return covers;
};

export const getDownloadArtworks = () => {
  const { downloadArtworks } = store.getState().downloads;
  const artworks = [];
  downloadArtworks.forEach((artwork) => {
    artworks.push({ ...artwork });
  });
  return artworks;
};

export const getAlbumSkuTracks = (album_sku) => {
  const state = store.getState();
  const albums = [];
  const { downloadAlbums } = state.downloads;

  if (downloadAlbums === undefined) return [];

  for (let i = 0; i < downloadAlbums.length; i += 1) {
    if (downloadAlbums[i].album_sku === album_sku)
      albums.push(downloadAlbums[i]);
  }
  return albums;
};

export const getAlbumTrackIndex = (track) => {
  const state = store.getState();
  const { downloadAlbums } = state.downloads;
  const index = downloadAlbums.findIndex((element) => {
    return (
      element.album_sku === track.album_sku &&
      element.track_number === track.track_number
    );
  });

  return index;
};

export const getFolderName = (album) => {
  const title = album.album_title.replace(/([<>:"/\\|?])+/g, ' ');
  const fileFolderName = `${title} - ${album.album_sku}`;
  const dir = `${getDownloadPath()}/${fileFolderName}`;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  return fileFolderName;
};

export const checkPath = (path: string): boolean => {
  if (!fs.existsSync(path)) return false;
  return true;
};

export const downloadCover = async (album, album_sku, cover_url) => {
  const downloadCovers = getDownloadCovers();
  const index = downloadCovers.findIndex((element) => {
    return element.album_sku === album_sku;
  });
  if (index > -1) {
    const covers = downloadCovers.splice(index, 1);
    store.dispatch(setDownloadCovers(covers));
  }
  let downloadPath = '';
  try {
    downloadPath = getDownloadPath();
  } catch (error) {
    store.dispatch(setDownloadCovers([]));
    throw Error(
      'The download location does not exist. This is probably caused by setting the download path to an external drive that has been removed afterwards.'
    );
  }
  const folderName = getFolderName(album);
  const filepath = `${downloadPath}/${folderName}/${album_sku}.jpg`;
  const response = await got.stream(cover_url, { method: 'GET' });
  response.pipe(fs.createWriteStream(filepath));

  response.on('downloadProgress', (progress) => {
    const covers = getDownloadCovers();
    const i = covers.findIndex((element) => {
      return element.album_sku === album_sku;
    });
    if (i < 0) return;

    covers[i].progress = progress.percent * 100;
    store.dispatch(setDownloadCovers(covers));
  });

  response.on('end', () => {
    const covers = getDownloadCovers();
    const i = covers.findIndex((element) => {
      return element.album_sku === album_sku;
    });
    covers[i].progress = 100;
    covers[i].downloading = false;
    covers[i].status = 'finished';
    store.dispatch(setDownloadCovers(covers));
  });
  response.on('error', () => {
    const covers = getDownloadCovers();
    const i = covers.findIndex((element) => {
      return element.album_sku === album_sku;
    });
    covers[i].status = 'error';
    store.dispatch(setDownloadCovers(covers));
  });
};

export const downloadArtwork = async (album, album_sku, artwork_url) => {
  const downloadArtworks = getDownloadArtworks();
  const index = downloadArtworks.findIndex((element) => {
    return element.album_sku === album_sku;
  });
  if (index > -1) {
    const artworks = downloadArtworks.splice(index, 1);
    store.dispatch(setDownloadArtworks(artworks));
  }
  let downloadPath = '';
  try {
    downloadPath = getDownloadPath();
  } catch (error) {
    store.dispatch(setDownloadArtworks([]));
    throw Error(
      'The download location does not exist. This is probably caused by setting the download path to an external drive that has been removed afterwards.'
    );
  }
  const folderName = getFolderName(album);
  const filepath = `${downloadPath}/${folderName}/${album_sku}.pdf`;
  const response = await got.stream(artwork_url, { method: 'GET' });
  const file = fs.createWriteStream(filepath);
  response.pipe(file);

  response.on('downloadProgress', (progress) => {
    const artworks = getDownloadArtworks();
    const i = artworks.findIndex((element) => {
      return element.album_sku === album_sku;
    });
    if (i < 0) return;
    artworks[i].progress = progress.percent * 100;
    store.dispatch(setDownloadArtworks(artworks));
  });

  response.on('end', () => {
    const artworks = getDownloadArtworks();
    const i = artworks.findIndex((element) => {
      return element.album_sku === album_sku;
    });
    artworks[i].progress = 100;
    artworks[i].downloading = false;
    artworks[i].status = 'finished';
    store.dispatch(setDownloadArtworks(artworks));
  });
  response.on('error', () => {
    const artworks = getDownloadArtworks();
    const i = artworks.findIndex((element) => {
      return element.album_sku === album_sku;
    });
    artworks[i].status = 'error';
    store.dispatch(setDownloadArtworks(artworks));
  });
};

export const cancelFileDownload = (track) => {
  const albums = getDownloadAlbums();
  albums[getAlbumTrackIndex(track)].status = 'aborted';
  store.dispatch(setDownloadAlbums(albums));
};

export const removeFileFromDownloadList = (track) => {
  const albums = getDownloadAlbums();
  const index = albums.findIndex((element) => {
    return (
      element.album_sku === track.album_sku &&
      element.track_number === track.track_number
    );
  });
  albums.splice(index, 1);
  store.dispatch(setDownloadAlbums(albums));
};
export const removeArtworkAndCover = () => {
  store.dispatch(setDownloadArtworks([]));
  store.dispatch(setDownloadCovers([]));
};

export const setTrackUrl = (track, url) => {
  const albums = getDownloadAlbums();
  albums[getAlbumTrackIndex(track)].url = url;
  store.dispatch(setDownloadAlbums(albums));
};

export const setErrorFileDownload = (track) => {
  const albums = getDownloadAlbums();
  albums[getAlbumTrackIndex(track)].status = 'error';
  store.dispatch(setDownloadAlbums(albums));
};

export const pauseFileDownload = (track) => {
  const albums = getDownloadAlbums();
  albums[getAlbumTrackIndex(track)].status = 'paused';
  store.dispatch(setDownloadAlbums(albums));
};

export const resumeFileDownload = (track) => {
  const albums = getDownloadAlbums();
  albums[getAlbumTrackIndex(track)].status = 'resumed';
  store.dispatch(setDownloadAlbums(albums));
};

const getFilePath = (track) => {
  let downloadPath = '';
  try {
    downloadPath = getDownloadPath();
  } catch (error) {
    removeFileFromDownloadList(track);
    throw Error(
      'The download location does not exist. This is probably caused by setting the download path to an external drive that has been removed afterwards.'
    );
  }

  const { url, soundFormat } = track;
  const title = track.track_title.replace(/([<>:"/\\|?])+/g, ' ');

  let extension = url.substring(url.lastIndexOf('/'), url.indexOf('?') + 1);
  extension = extension.substring(
    extension.indexOf('.'),
    extension.indexOf('?')
  );
  const fileName = `${track.track_number}. ${title} ${soundFormat.name}${extension}`;

  const folder = getFolderName(track);
  const dir = `${downloadPath}/${folder}/${soundFormat.name}`;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  const filePath = `${downloadPath}/${folder}/${soundFormat.name}/${fileName}`;
  return { filePath, downloadPath };
};

export const downloadFile = async (track, callback) => {
  if (getCurrentDownloadingCount() > 0) return;
  let downloadAlbums = getDownloadAlbums();

  downloadAlbums[getAlbumTrackIndex(track)].status = 'downloading';
  store.dispatch(setDownloadAlbums(downloadAlbums));

  const { url } = track;

  const { downloadPath, filePath } = getFilePath(track);

  const file = fs.createWriteStream(filePath);

  try {
    const response = await got.stream(url, { method: 'GET' });
    // response.pipe(file);
    downloadAlbums = getDownloadAlbums();
    const interval = setInterval(() => {
      const albums = getDownloadAlbums();
      if (
        albums[getAlbumTrackIndex(track)].status !== 'aborted' &&
        response.destroyed
      ) {
        albums[getAlbumTrackIndex(track)].status = 'error';
        store.dispatch(setDownloadAlbums(albums));
      }
      if (albums[getAlbumTrackIndex(track)].status === 'resumed') {
        response.resume();
      }
      if (albums[getAlbumTrackIndex(track)].status === 'aborted') {
        // response.unpipe(file);
        response.destroy();
        file.destroy();
      }
      if (!checkPath(downloadPath)) {
        albums[getAlbumTrackIndex(track)].status = 'error';
        store.dispatch(setDownloadAlbums(albums));
        response.unpipe(file);
        response.destroy();
        clearInterval(interval);
      }
    }, 1000);
    response.on('downloadProgress', (progress) => {
      const albums = getDownloadAlbums();
      if (
        albums[getAlbumTrackIndex(track)] &&
        albums[getAlbumTrackIndex(track)].status !== 'paused' &&
        albums[getAlbumTrackIndex(track)].status !== 'aborted' &&
        albums[getAlbumTrackIndex(track)].status !== 'error'
      ) {
        albums[getAlbumTrackIndex(track)].progress = progress.percent * 100;
        albums[getAlbumTrackIndex(track)].status = 'progress';
        store.dispatch(setDownloadAlbums(albums));
      }
    });
    response.on('data', (chunk) => {
      file.write(chunk);
      const albums = getDownloadAlbums();
      if (
        albums[getAlbumTrackIndex(track)] &&
        albums[getAlbumTrackIndex(track)].status !== 'paused' &&
        albums[getAlbumTrackIndex(track)].status !== 'aborted' &&
        albums[getAlbumTrackIndex(track)].status !== 'error'
      ) {
        albums[getAlbumTrackIndex(track)].bytes += chunk.length;
        store.dispatch(setDownloadAlbums(albums));
      }
      if (albums[getAlbumTrackIndex(track)].status === 'aborted') {
        response.destroy();
        file.destroy();
      }
      if (albums[getAlbumTrackIndex(track)].status === 'paused') {
        response.pause();
      }
      if (albums[getAlbumTrackIndex(track)].status === 'resumed') {
        response.resume();
      }
    });
    response.on('close', () => {
      callback(true);
      const albums = getDownloadAlbums();
      fs.unlink(filePath, (error) => {
        console.log(error);
      });
      if (albums[getAlbumTrackIndex(track)].status !== 'error')
        removeFileFromDownloadList(track);
      clearInterval(interval);
    });
    response.on('error', () => {
      const albums = getDownloadAlbums();
      albums[getAlbumTrackIndex(track)].status = 'error';
      store.dispatch(setDownloadAlbums(albums));
    });
    response.on('end', () => {
      const albums = getDownloadAlbums();
      albums[getAlbumTrackIndex(track)].status = 'finished';
      albums[getAlbumTrackIndex(track)].downloadDate = Date.now();
      ipcRenderer.send(
        'notify',
        ` Track ${track.track_number} of album ${track.album_sku} is downloaded!`
      );
      store.dispatch(setDownloadAlbums(albums));
      clearInterval(interval);
      file.close();
      callback(true);
    });
  } catch (error) {
    setErrorFileDownload(track);
    throw Error(
      `Cannot load tracks ${track.track_number} at this time. Please contact support.`
    );
  }
};

export const onDownloadFinish = () => {
  if (getCurrentDownloadingCount() > 1) return;

  const downloadTracks = store.getState().downloads.downloadAlbums;

  for (let i = 0; i < downloadTracks.length; i += 1) {
    if (downloadTracks[i].status === 'queued') {
      downloadFile(downloadTracks[i], onDownloadFinish);
      i = downloadTracks.length;
    } else if (downloadTracks[i].status === 'finished') {
      console.log(
        'onDownloadFinish key',
        `${downloadTracks[i].album_sku}_${downloadTracks[i].track_number}`
      );
      electronStore.set(
        `${downloadTracks[i].album_sku}_${downloadTracks[i].track_number}`,
        true
      );
    }
  }
};

export const clearDownloads = () => {
  const downloadTracks = getDownloadAlbums();
  downloadTracks.forEach((element: any) => {
    cancelFileDownload(element);
    if (
      element.status === 'finished' ||
      element.status === 'queued' ||
      element.status === 'error'
    )
      removeFileFromDownloadList(element);
  });
  removeArtworkAndCover();
};
