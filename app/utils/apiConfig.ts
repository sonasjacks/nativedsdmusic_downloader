import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import store from './storage';

const instance = axios.create({
  baseURL: 'https://api.nativedsd.com/v2/',
  auth: {
    username: process.env.API_KEY,
    password: process.env.API_PASSWORD,
  },
});
const accessToken = store.get('access_token', '');
instance.defaults.headers.common['X-ACCESS-TOKEN'] = accessToken;
export default instance;
