export interface Album {
  album_booklet: string;
  album_cover: string;
  album_sku: string;
  album_title: string;
  sound_formats: SoundFormats;
  subscription_end_date: string;
  subscription_status: string;
  tracks: Track[];
}

export interface Catalog {
  album: Album[];
  count: number;
}

export interface SoundFormats {
  stereo: string;
  stereo128: string;
}

export interface Track {
  track_number: string;
  track_title: string;
}
