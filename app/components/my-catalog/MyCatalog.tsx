/* eslint-disable no-shadow */
import React, { useEffect, useState, useRef } from 'react';
import ScrollArea from 'react-scrollbar';
import { ClassicSpinner } from 'react-spinners-kit';
import { Button } from 'antd';
import axios, { AxiosResponse } from 'axios';
import background from '../../assets/background.svg';

/** Components */
import Sidebar from '../sidebar/Sidebar';
import Popup from '../popup/Popup';

/** Models */
import { Album, Catalog } from './MyCatalog.model';

/** Styles */
import styles from './MyCatalog.module.less';

/** Utils */
import store from '../../utils/storage';
import instance from '../../utils/apiConfig';

export default function MyCatalog(props: any): JSX.Element {
  const [albums, setAlbums] = useState<Album[]>([]);
  const [showPopup, setShowPopup] = useState(false);
  const [showSettingsPopup, setShowSeetingsPopup] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [limit, setLimit] = useState(12);
  const [sortOption, setSortOption] = useState();

  const [loadedImages, setLoadedImages] = useState(0);
  const scrollbarStyles = { borderRadius: 5 };
  let timer: any;
  const { history } = props;
  const lastClickedAlbum = useRef(null);
  const container = useRef(null);

  const handleClickAlbum = (album: Album) => {
    history.push({
      pathname: `/detail/${album.uuid}`,
      state: {
        album,
        limit,
      },
    });
  };

  const handleKeyDownAlbum = (album: Album) => {
    history.push({
      pathname: `/detail/${album.uuid}`,
      state: {
        album,
      },
    });
  };

  const onLogOut = () => {
    setShowPopup(true);
  };

  const onLoadMore = () => {
    setLimit(limit + 12);
  };

  const togglePopup = () => {
    setShowPopup(false);
    setShowSeetingsPopup(false);
  };

  const onImagesLoaded = () => {
    const allImages: any = document.querySelectorAll('img');
    let loaded = true;

    allImages.forEach((element: any) => {
      if (!element.complete) {
        loaded = false;
      }
    });

    return loaded;
  };

  const renderLoading = () => {
    return isLoading ? (
      <div className={styles.divLoadingBar}>
        <ClassicSpinner size={30} color="#686769" loading={isLoading} />
      </div>
    ) : null;
  };

  const sortAlbums = (option: string, albums: Album[]): Album[] => {
    switch (option) {
      case 'alphabetical':
        return albums.sort((a: Album, b: Album) => {
          if (a.album_title > b.album_title) return 1;
          if (a.album_title < b.album_title) return -1;
          return 0;
        });
        break;
      case 'default':
        return albums;
        break;
      default:
        return albums;
        break;
    }
  };

  const loadAlbums = () => {
    const url = '/albums/';
    const sortOption = store.get('sortOption', 'default');
    setSortOption(sortOption);
    instance
      .get(url, {})
      .then((response: AxiosResponse<Catalog>) => {
        const albums = sortAlbums(sortOption, response.data.albums);
        setAlbums(albums);
        setIsLoading(false);
        setLimit(props.location.state?.limit || 12);
        if (props.location.state?.action === 'showSettingsPopup') {
          setShowSeetingsPopup(true);
        }
        container.current.scrollTo(
          0,
          lastClickedAlbum.current.offsetTop - container.current.offsetTop
        );
        return response.data.albums;
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const setImageLoadListener = () => {
    const allImages: any = document.querySelectorAll('img');

    allImages.forEach((element: HTMLImageElement) => {
      element.onload = () => {
        setLoadedImages(loadedImages + 1);

        if (loadedImages > 12 && onImagesLoaded()) {
          // if (messagesEnd) {
          //   messagesEnd.scrollBottom();
          // }
        }
      };
    });
  };

  useEffect(() => {
    let mounted = true;
    setImageLoadListener();
    return () => {
      mounted = false;
    };
  }, [limit, albums]);

  useEffect((): any => {
    let mounted = true;
    const accessToken = store.get('access_token', '');
    const isLoggedIn = store.get('isLoggedIn', '');
    if (!isLoggedIn) {
      history.push('/');
    }

    instance.defaults.headers.common['X-ACCESS-TOKEN'] = accessToken;

    if (mounted) loadAlbums();
    timer = setInterval(loadAlbums, 1000 * 30);
    return () => {
      clearInterval(timer);
      mounted = false;
    };
  }, []);

  const onSortAlbums = (event): void => {
    store.set('sortOption', event.target.value);
    setIsLoading(true);
    loadAlbums();
  };

  return (
    <div className={styles.container}>
      <Sidebar history={history} onLogOut={onLogOut} />
      {renderLoading()}
      <h1>My Catalog</h1>
      <div className={styles.sortWrapper}>
        <span>Sort by</span>
        <select onChange={onSortAlbums} value={sortOption}>
          <option value="alphabetical"> alphabet</option>
          <option value="default"> purchase date</option>
        </select>
        <i className="fas fa-chevron-down" />
      </div>
      <div className={styles.scrollarea} ref={container}>
        {albums.slice(0, limit).map((album: any, index: number) => (
          <div
            className={styles.album}
            id={album.album_sku}
            ref={
              history.location.state?.scrollTo === album.album_sku
                ? lastClickedAlbum
                : null
            }
            key={`album-${album.album_sku}`}
            role="button"
            tabIndex={index}
            onClick={() => handleClickAlbum(album)}
            onKeyDown={() => handleKeyDownAlbum(album)}
          >
            <div className={styles.albumImg}>
              <img id={`img_${index}`} alt="album" src={album.album_cover} />
            </div>
            <h2 className={styles.title}>{album.album_title}</h2>
          </div>
        ))}
        {limit < albums.length && (
          <div className={styles.btnWrapper}>
            <button
              type="button"
              className={styles.loadBtn}
              onClick={onLoadMore}
            >
              Load More
            </button>
          </div>
        )}
      </div>

      {showPopup && (
        <Popup props={props} type="logoutPopup" closePopup={togglePopup} />
      )}
      {showSettingsPopup && (
        <Popup props={props} type="settingsPopup" closePopup={togglePopup} />
      )}
      <img src={background} alt="" className={styles.backgroundImg} />
    </div>
  );
}
