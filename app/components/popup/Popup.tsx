import React from 'react';
import { Button } from 'antd';

/** Styles */
import styles from './Popup.module.less';
import background from '../../assets/background.svg';

/** Utils */
import store from '../../utils/storage';
import {
  cancelFileDownload,
  removeFileFromDownloadList,
  removeArtworkAndCover,
  clearDownloads,
} from '../../utils/downloader';
import routes from '../../constants/routes.json';

export default function Popup(props: any) {
  const { closePopup, type } = props;
  const onConfirm = () => {
    if (type === 'logoutPopup') {
      store.set('isLoggedIn', false);
      props.props.history.replace('/');
      clearDownloads();
    } else if (type === 'settingsPopup') {
      props.closePopup();
      props.props.history.replace(routes.SETTINGS);
    }
  };

  return (
    <div className={styles.popup}>
      <div className={styles.popup_inner}>
        <img src={background} className={styles.backgroundImg} alt="" />
        <h1 className={styles.title}>
          {type === 'logoutPopup' && 'Are you sure you want to log out?'}
          {type === 'settingsPopup' && 'Check the location of your downloads'}
        </h1>
        {type === 'logoutPopup' && (
          <p>
            If you log out now, all your download history will be lost and any
            active downloads will abort.
          </p>
        )}
        {type === 'settingsPopup' && (
          <p>
            Please visit the settings page in order to set the desired download
            location of your music.
          </p>
        )}
        <div className={styles.buttonsWrapper}>
          <button
            type="button"
            className={styles.cancelBtn}
            onClick={closePopup}
          >
            Cancel
          </button>
          <button
            type="button"
            className={styles.confirmBtn}
            onClick={onConfirm}
          >
            {type === 'logoutPopup' ? 'Log out' : ''}
            {type === 'settingsPopup' ? 'Go to settings' : ''}
          </button>
        </div>
      </div>
    </div>
  );
}
