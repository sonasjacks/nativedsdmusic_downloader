// @flow
import React, { useEffect, useState } from 'react';

/** Assets */
import background from '../../assets/background.svg';

/** Styles */
import styles from './DownloadList.module.less';

/** Components */
import DownloadTrack from '../download-track/DownloadTrack';
import Popup from '../popup/Popup';
import Sidebar from '../sidebar/Sidebar';

/** Models */
import { DownloadTrack as DownloadTrackModel } from '../download-track/DownloadTrack.model';

export default function DownloadList(props: any): JSX.Element {
  const [downloadTracks, setDownloadTracks] = useState<DownloadTrackModel[]>(
    []
  );
  const [showPopup, setShowPopup] = useState(false);
  const { history } = props;

  const onLogOut = () => {
    setShowPopup(true);
  };
  const togglePopup = () => {
    setShowPopup(!showPopup);
  };

  const getDownloadTracks = () => {
    const { downloadAlbums } = props;
    const tracks = downloadAlbums.reduce((r, element) => {
      r[element.album_sku] = r[element.album_sku] || [];
      r[element.album_sku].push(element);
      return r;
    }, {});
    setDownloadTracks(tracks);
  };

  useEffect(() => {
    getDownloadTracks();
  }, [props]);

  const renderTracks = (tracks: DownloadTrack) => {
    return (
      tracks.length > 0 &&
      tracks.map((track, index) => (
        <DownloadTrack
          key={`${track.track_number}${track.album_sku}${track.soundFormat.name}`}
          track={track}
          layout={1}
        />
      ))
    );
  };

  return (
    <div className={styles.container}>
      <h1>Downloads</h1>
      <Sidebar history={history} onLogOut={onLogOut} />
      {Object.keys(downloadTracks).length > 0 ? (
        <div>
          <div className={styles.header}>
            <h4>TRACK NAME</h4>
            <h4>DOWNLOAD DATE</h4>
          </div>
          <div className={styles.scrollArea}>
            {downloadTracks &&
              Object.keys(downloadTracks).map((key, index) =>
                renderTracks(downloadTracks[key])
              )}
          </div>
        </div>
      ) : (
        <div>
          <h3> No tracks have been selected for download.</h3>
        </div>
      )}
      {showPopup && (
        <Popup props={props} type="logoutPopup" closePopup={togglePopup} />
      )}
      <img src={background} alt="" className={styles.backgroundImg} />
    </div>
  );
}
