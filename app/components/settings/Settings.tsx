// @flow
import React, { useState, useEffect, useContext } from 'react';
import { Button, Input, Select } from 'antd';
import { ipcRenderer } from 'electron';
import downloadLocationIcon from '../../assets/icons/download-location-icon.svg';
import channelIcon from '../../assets/icons/channel-icon.svg';
import browseIcon from '../../assets/icons/browse-icon.svg';
import notificationIcon from '../../assets/icons/notification-icon.svg';
import background from '../../assets/background.svg';

/** Components */
import Sidebar from '../sidebar/Sidebar';
import Popup from '../popup/Popup';

/** Styles */
import styles from './Settings.module.less';

/** Utils */
import { getDownloadPath } from '../../utils/downloader';
import store from '../../utils/storage';
import { ErrorContext } from '../../contexts/ErrorContext';

export default function Settings(props: any) {
  const [showPopup, setShowPopup] = useState(false);
  const [notificationPermission, setNotificationPermission] = useState(true);
  const [defaultChannel, setDefaultChannel] = useState('');
  const [downloadPath, setDownloadPath] = useState('');
  const [channels, setChannels] = useState({
    multi512: 'Multi 512fs',
    multi256: 'Multi 256fs',
    multi128: 'Multi 128fs',
    multi: 'Multi 64fs',
    multiDXD: 'Multi DXD',
    stereo512: 'Stereo 512fs',
    stereo256: 'Stereo 256fs',
    stereo128: 'Stereo 128fs',
    stereo: 'Stereo 64fs',
    stereoDXD: 'Stereo DXD',
    binaural512: 'Binaural 512fs',
    binaural256: 'Binaural 256fs',
    binaural128: 'Binaural 128fs',
    binaural: 'Binaural 64fs',
    binauralDXD: 'Binaural DXD',
    sonorus512: 'Sonorus 512fs',
    sonorus256: 'Sonorus 256fs',
    sonorus128: 'Sonorus 128fs',
    sonorus: 'Sonorus 64fs',
    sonorusDXD: 'Sonorus DXD',
  });
  const { history } = props;
  const { error, setError } = useContext(ErrorContext);

  useEffect(() => {
    ipcRenderer.on('selected-file', (event, paths) => {
      setDownloadPath(paths[0]);
    });
    try {
      const path = getDownloadPath();
      store.set('downloadLocation', path);
      setDownloadPath(path);
    } catch (err) {
      setDownloadPath(store.get('downloadLocation'));
      setError(
        Error(
          'The download location does not exist. This is probably caused by setting the download path to an external drive that has been removed afterwards.'
        )
      );
    }

    const channel = store.get('defaultChannel', 'stereo');
    if (channel !== '') {
      setDefaultChannel(channel);
    }

    const notifications = store.get('notificationPermission', true);
    setNotificationPermission(notifications);
  }, [setDownloadPath]);

  const onSave = () => {
    store.set('downloadLocation', downloadPath);
    store.set('defaultChannel', defaultChannel);
    store.set('notificationPermission', notificationPermission);
    const params = {
      pathname: props.location.state?.pathname,
      state: {
        album: props.history.location.state?.album,
      },
    };

    props.history.replace(params);
  };

  const onLogOut = () => {
    setShowPopup(true);
  };

  const togglePopup = () => {
    setShowPopup(!showPopup);
  };

  const onCancel = () => {
    setDownloadPath(store.get('downloadLocation'));
    setDefaultChannel(store.get('defaultChannel'));
    setNotificationPermission(store.get('notificationPermission'));
  };

  const onBrowse = () => {
    ipcRenderer.send('open-file-dialog');
  };

  const onDefaultChannelChange = (e: any) => {
    setDefaultChannel(e.target.value);
  };

  const checkForChanges = (): boolean => {
    return (
      store.get('downloadLocation') !== downloadPath ||
      store.get('defaultChannel') !== defaultChannel ||
      store.get('notificationPermission') !== notificationPermission
    );
  };

  const toggleNotificationPermissions = (value: boolean) => {
    setNotificationPermission(value);
  };

  return (
    <div className={styles.container}>
      <h1>Settings</h1>
      <Sidebar history={history} onLogOut={onLogOut} />

      <div className={styles.row}>
        <img src={downloadLocationIcon} alt="" className={styles.icon} />
        <div>
          <h3>Default download location</h3>
          <h4>{downloadPath}</h4>
        </div>
        <button type="button" className={styles.btnBrowse} onClick={onBrowse}>
          <span>Browse...</span>
          <img src={browseIcon} alt="" />
        </button>
      </div>

      {/* <div className={styles.row}>
        <img src={channelIcon} alt="" className={styles.icon} />
        <div>
          <h3>Channel and Quality pairing</h3>
          <h4>
            If desired Default Channel & Quality Setting pairing is not
            available, please select from available pairings on Album download
            screen.
          </h4>
        </div>
        <select
          onChange={onDefaultChannelChange}
          value={defaultChannel}
          className={styles.select}
          placeholder="Select Channels"
        >
          {channels &&
            Object.keys(channels).map((key) => (
              <option value={key} key={key}>
                {channels[key]}
              </option>
            ))}
        </select>
      </div> */}
      <div className={styles.row}>
        <img src={notificationIcon} alt="" className={styles.icon} />
        <div>
          <h3>Notifications</h3>
          <h4>
            Show notifications on your screen when a track completes
            downloading. You may need to allow notifications from the NativeDSD
            Downloader in your computer system settings.
          </h4>
        </div>
        <div className={styles.toggleSwitch}>
          <div
            onClick={toggleNotificationPermissions.bind(null, true)}
            onKeyUp={toggleNotificationPermissions.bind(null, true)}
            role="button"
            tabIndex={0}
            className={`${notificationPermission ? styles.enabled : ''}`}
          >
            On
          </div>
          <div
            onClick={toggleNotificationPermissions.bind(null, false)}
            onKeyUp={toggleNotificationPermissions.bind(null, false)}
            role="button"
            tabIndex={0}
            className={`${notificationPermission ? '' : styles.enabled}`}
          >
            Off
          </div>
        </div>
      </div>

      {checkForChanges() && (
        <div className={styles.buttonsWrapper}>
          <div>
            <button
              type="button"
              className={styles.cancelBtn}
              onClick={onCancel}
            >
              Cancel
            </button>
          </div>
          <div>
            <button type="button" className={styles.saveBtn} onClick={onSave}>
              Save Settings
            </button>
          </div>
        </div>
      )}
      <img src={background} alt="" className={styles.backgroundImg} />
      {showPopup && (
        <Popup props={props} type="logoutPopup" closePopup={togglePopup} />
      )}
    </div>
  );
}
