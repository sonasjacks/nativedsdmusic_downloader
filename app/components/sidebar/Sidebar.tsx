// @flow
import React, { useEffect, useState } from 'react';
import { Button, Divider, Menu } from 'antd';
import { NavLink } from 'react-router-dom';
import { UserOutlined } from '@ant-design/icons';

/** Assets */
import imgLogo from '../../assets/logo.svg';
import downloadsIcon from '../../assets/icons/downloads-icon.svg';
import settingsIcon from '../../assets/icons/settings-icon.svg';
import catalogIcon from '../../assets/icons/catalog-icon.svg';
import activeDownloadsIcon from '../../assets/icons/active-downloads-icon.svg';
import activeCatalogIcon from '../../assets/icons/active-catalog-icon.svg';
import activeSettingsIcon from '../../assets/icons/active-settings-icon.svg';

/** Styles */
import styles from './Sidebar.module.less';

/** Utils */
import store from '../../utils/storage';

export default function Sidebar(props: any): JSX.Element {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [firstName, setFirstName] = useState('');

  useEffect(() => {
    setLoggedIn(store.get('isLoggedIn', false));
    setFirstName(store.get('firstname', ''));
  }, []);

  const clickOnLogo = () => {
    props.history.push({
      pathname: '/',
    });
  };

  const onLogout = () => {
    props.onLogOut();
  };

  const renderTitle = () => {
    return (
      <div>
        <UserOutlined id={styles.userIcon} />
        <h3>{firstName}</h3>
      </div>
    );
  };

  const isCurrentPath = (path: string): boolean => {
    return props.history.location.pathname === path;
  };

  return (
    <div className={styles.sidebar}>
      <div>
        {isLoggedIn ? (
          <div>
            <Menu mode="inline">
              <Menu.SubMenu className={styles.submenu} title={renderTitle()}>
                <Menu.Item onClick={onLogout} className={styles.submenuBtn}>
                  Log Out
                </Menu.Item>
              </Menu.SubMenu>
            </Menu>
            <div className={styles.buttonWrapper}>
              <NavLink
                exact
                to="/my-catalog"
                activeClassName={styles.active}
                className={styles.menuButton}
              >
                <img
                  src={
                    isCurrentPath('/my-catalog')
                      ? activeCatalogIcon
                      : catalogIcon
                  }
                  alt=""
                />
                My Catalog
              </NavLink>

              <NavLink
                exact
                to="/downloads"
                activeClassName={styles.active}
                className={styles.menuButton}
              >
                <img
                  src={
                    isCurrentPath('/downloads')
                      ? activeDownloadsIcon
                      : downloadsIcon
                  }
                  alt=""
                />
                Downloads
              </NavLink>

              <NavLink
                exact
                to="/settings"
                activeClassName={styles.active}
                className={styles.menuButton}
              >
                <img
                  src={
                    isCurrentPath('/settings')
                      ? activeSettingsIcon
                      : settingsIcon
                  }
                  alt=""
                />
                Settings
              </NavLink>
            </div>
          </div>
        ) : null}
        <div
          onClick={clickOnLogo}
          className={styles.logo}
          onKeyUp={clickOnLogo}
          tabIndex="0"
          role="button"
        >
          <img alt="logo" src={imgLogo} />
        </div>
      </div>
    </div>
  );
}
