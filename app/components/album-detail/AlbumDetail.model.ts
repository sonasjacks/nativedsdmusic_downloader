export interface Album {
  uuid: string;
  album_booklet: string;
  album_cover: string;
  album_sku: string;
  album_title: string;
  artists: string[];
  sound_formats: SoundFormats[];
  subscription_end_date: string;
  subscription_status: string;
  tracks: Track[];
}
export interface SoundFormats {
  download_uuid: string;
  download_format: string;
}
export interface Track {
  track_number: string;
  track_title: string;
  track_length: string;
  track_preview: string;
}
