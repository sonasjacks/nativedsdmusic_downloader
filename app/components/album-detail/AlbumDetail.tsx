/* eslint-disable no-plusplus */
// @flow
/* eslint-disable react/no-array-index-key */
/* eslint-disable no-shadow */
import React, { useState, useEffect, useContext } from 'react';
import { ClassicSpinner } from 'react-spinners-kit';
import { Button, Checkbox, Progress } from 'antd';
import { AxiosResponse } from 'axios';
import { useParams } from 'react-router-dom';

/** Assets */
import background from '../../assets/background-white.svg';
import artworkIcon from '../../assets/icons/artwork-icon.svg';
import coverIcon from '../../assets/icons/cover-icon.svg';

/** Components */
import Popup from '../popup/Popup';
import DownloadTrack from '../download-track/DownloadTrack';

/** Models */
import { Album, Track } from './AlbumDetail.model';
import { DownloadTrack as DownloadTrackModel } from '../download-track/DownloadTrack.model';

/** Styles */
import styles from './AlbumDetail.module.less';

/** Utils */
import {
  downloadFile,
  getAlbumSkuTracks,
  downloadArtwork as getDownloadArtwork,
  downloadCover as getDownloadCover,
  getCurrentDownloadingCount,
  removeFileFromDownloadList,
  getDownloadAlbums,
  setErrorFileDownload,
} from '../../utils/downloader';
import store from '../../utils/storage';
import { ErrorContext } from '../../contexts/ErrorContext';
import instance from '../../utils/apiConfig';

export default function AlbumDetail(props: any): JSX.Element {
  const [album, setAlbum] = useState<Album>(null);
  const [albumTracks, setAlbumTracks] = useState<Track[]>([]);
  const [chkStates, setChkStates] = useState<boolean[]>([]);
  const [defaultChannel, setDefaultChannel] = useState('');
  const [downloadArtwork, setDownloadArtwork] = useState('');
  const [downloadCover, setDownloadCover] = useState(null);
  const [downloadTracks, setDownloadTracks] = useState<DownloadTrackModel[]>(
    null
  );
  const [isLoading, setIsLoading] = useState(true);
  const [showFooter, setShowFooter] = useState(true);
  const [showPopup, setShowPopup] = useState(false);
  const [soundFormat, setSoundFormat] = useState({ id: '', name: '' });
  const { history } = props;
  const { id } = useParams();
  const { error, setError } = useContext(ErrorContext);

  const filterTracks = (): void => {
    let filteredTracks: Track[] = album?.tracks;
    if (album && downloadTracks) {
      filteredTracks = album?.tracks.filter((track) => {
        return !downloadTracks.find((downloadTrack) => {
          return (
            downloadTrack.track_number === track.track_number &&
            downloadTrack.status !== 'finished'
          );
        });
      });
      setAlbumTracks(filteredTracks);
    }
  };

  const getDownloadTracks = (): void => {
    const tracks = getAlbumSkuTracks(album?.album_sku);
    setDownloadTracks(tracks);
  };

  const getDownloadingCover = (): void => {
    const { downloadCovers } = props;
    if (album) {
      const index = downloadCovers.findIndex((element) => {
        return element.album_sku === album.album_sku;
      });

      let cover;
      if (index > -1) cover = downloadCovers[index];

      setDownloadCover(cover);
    }
  };

  const getDownloadingArtwork = (): void => {
    const { downloadArtworks } = props;
    if (album) {
      const index = downloadArtworks.findIndex((element) => {
        return element.album_sku === album.album_sku;
      });

      let artwork;
      if (index > -1) artwork = downloadArtworks[index];
      setDownloadArtwork(artwork);
    }
  };

  const getAlbum = (id: string): void => {
    const url = `/albums/${id}`;
    instance
      .get(url, {})
      .then((response: AxiosResponse<Album>) => {
        setAlbum(response.data);
        setIsLoading(false);
        for (let i = 0; i < response.data.tracks.length; i += 1) {
          chkStates.push(false);
        }
        chkStates.fill(false, 0, response.data.tracks.length);
        setChkStates([...chkStates]);
        if (response.data.sound_formats.length === 1) {
          setSoundFormat({
            id: response.data.sound_formats[0].download_uuid,
            name: response.data.sound_formats[0].download_format,
          });
        }
        return response.data;
      })
      .catch((err) => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    try {
      // setAlbum(props.location.state.album);
      getAlbum(id);
      filterTracks();
    } catch (err) {
      console.log(err);
    }
  }, []);

  useEffect(() => {
    getDownloadTracks();
    getDownloadingArtwork();
    getDownloadingCover();
  }, [props, album]);

  useEffect(() => {
    filterTracks();
  }, [downloadTracks, album]);

  const togglePopup = () => {
    setShowPopup(!showPopup);
  };

  const onTickChange = (e: CheckboxChangeEvent): void => {
    const id = Number(e.target.id);
    chkStates[id] = e.target.checked;
    setChkStates([...chkStates]);
    setShowFooter(true);
  };

  const onQualityChange = (e): void => {
    const name = album.sound_formats.find(
      (soundFormat) => soundFormat.download_uuid === e.target.value
    )?.download_format;
    setSoundFormat({ id: e.target.value, name });
  };

  const onDownloadFinish = (): void => {
    if (getCurrentDownloadingCount() > 1) return;

    const tracks = getDownloadAlbums();

    const nextTrack = tracks.find((track) => track.status === 'queued');

    if (nextTrack) {
      downloadFile(nextTrack, onDownloadFinish).catch((err) => {
        setError(err);
        setErrorFileDownload(nextTrack);
        onDownloadFinish();
      });
    }
  };

  const downloadFiles = async (trackIndexes: number[]) => {
    const tracks = [];
    const trackNumbers = [];

    trackIndexes.forEach((trackIndex) => {
      console.log(albumTracks[trackIndex]);
      const track = JSON.parse(JSON.stringify(albumTracks[trackIndex]));
      const existingDownloadTrack = downloadTracks.find(
        (downloadTrack: DownloadTrackModel) =>
          downloadTrack.track_number === track.track_number
      );
      if (existingDownloadTrack) {
        removeFileFromDownloadList(existingDownloadTrack);
      }
      track.album_sku = album.album_sku;
      track.album_cover = album.album_cover;
      track.album_title = album.album_title;
      track.artists = album.artists || [];
      track.status = 'queued';
      track.progress = 0;
      track.counter = 0;
      track.request = null;
      track.bytes = 0;
      track.automatic = false;
      track.soundFormat = soundFormat;
      tracks.push(track);
      trackNumbers.push(albumTracks[trackIndex].track_number);
    });

    if (trackNumbers.length === 0) {
      const notification = new Notification('NativeCore', {
        body: ` Tracks are unavailable to download`,
      });
      return;
    }

    try {
      const response = await instance.post(
        `https://api.nativedsd.com/v2/downloads/${soundFormat.id}`,
        {
          track_numbers: trackNumbers,
        }
      );

      const { count } = response.data;
      response.data.files.forEach((file, index) => {
        const trackNumber: string = file
          ?.substring(file.lastIndexOf('/') + 1, file.indexOf('?') + 1)
          .slice(0, 2);
        const track = tracks.find(
          (track) => parseFloat(track.track_number) === parseFloat(trackNumber)
        );
        if (track) track.url = file;
      });
      if (count > 0) {
        props.addDownloadAlbums(tracks);
        chkStates.fill(false);
        setChkStates([...chkStates]);
        const errorTracks = tracks.filter((track) => track.url === undefined);
        if (errorTracks.length > 0) {
          errorTracks.forEach((errorTrack) => {
            setErrorFileDownload(errorTrack);
          });
          const errorTracksNumbers = errorTracks.map(
            (errorTrack) => errorTrack.track_number
          );
          setError(
            Error(
              `Cannot load tracks ${errorTracksNumbers.toString()} at this time. Please contact support.`
            )
          );
        }
      } else {
        const notification = new Notification('NativeCore', {
          body: ` Tracks are unavailable to download`,
        });
        return;
      }

      for (let i = 0; i < tracks.length; i += 1) {
        if (tracks[i].url) {
          downloadFile(tracks[i], onDownloadFinish).catch((err) => {
            setError(err);
            setErrorFileDownload(tracks[i]);
            onDownloadFinish();
          });
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onStartDownload = async (): Promise<void> => {
    setShowFooter(false);
    const trackNumbers = chkStates
      .map((value, index) => {
        if (value) return index;
        return false;
      })
      .filter((value) => value !== false);
    await downloadFiles(trackNumbers);
  };

  const onDownloadCover = (): void => {
    if (downloadCover && downloadCover.downloading) return;
    props.addDownloadCover({
      album_sku: album.album_sku,
      progress: 0,
      downloading: true,
      status: 'downloading',
    });
    getDownloadCover(album, album.album_sku, album.album_cover).catch((err) => {
      setError(err);
    });
  };

  const onDownloadArtwork = (): void => {
    if (downloadArtwork && downloadArtwork.downloading) return;
    props.addDownloadArtwork({
      album_sku: album.album_sku,
      progress: 0,
      downloading: true,
      status: 'downloading',
    });
    getDownloadArtwork(album, album.album_sku, album.album_booklet).catch(
      (err) => {
        setError(err);
      }
    );
  };

  const selectAll = (): void => {
    if (albumTracks)
      for (let i = 0; i < albumTracks.length; i += 1) {
        chkStates[i] = true;
      }
    setChkStates([...chkStates]);
  };

  const selectNone = (): void => {
    chkStates.fill(false);
    setChkStates([...chkStates]);
  };

  const isChecked = (): boolean => {
    let trackCount = 0;
    if (albumTracks) {
      chkStates.forEach((chk) => {
        if (chk === true) trackCount += 1;
      });
      return trackCount === albumTracks.length;
    }
    return false;
  };

  const toggleSelect = (): void => {
    if (isChecked()) selectNone();
    else selectAll();
    setShowFooter(true);
  };

  const renderLoading = (): JSX.Element => {
    if (isLoading)
      return (
        <div className={styles.divLoadingBar}>
          <ClassicSpinner size={30} color="#686769" loading={isLoading} />
        </div>
      );
    return null;
  };

  const getSelectedTracks = (): number => {
    let num = 0;
    for (let i = 0; i < chkStates.length; i += 1) {
      if (chkStates[i]) num += 1;
    }
    return num;
  };

  const onOverview = (): void => {
    history.replace({
      pathname: `/my-catalog`,
      state: {
        scrollTo: album?.album_sku,
        limit: history.location?.state.limit,
      },
    });
  };

  const renderTrack = (
    track: Track | DownloadTrackModel,
    index: any
  ): JSX.Element => {
    const downloadTrack = downloadTracks.find(
      (element) => element.track_number === track.track_number
    );
    if (downloadTrack && downloadTrack.status !== 'finished')
      return (
        <DownloadTrack
          key={`${track.track_number}${track.album_sku}`}
          track={downloadTrack}
          layout={2}
        />
      );
    return (
      <div key={track.track_number} className={styles.track}>
        <Checkbox
          id={String(albumTracks.indexOf(track))}
          checked={chkStates[albumTracks.indexOf(track)]}
          onChange={onTickChange}
          className={styles.checkbox}
          value={track.track_number}
        >
          <h4>{`${track.track_number}   ${track.track_title}`}</h4>
        </Checkbox>
        {downloadTrack && downloadTrack.status === 'finished' && (
          <i className={`fas fa-check ${styles.downloaded}`} />
        )}
      </div>
    );
  };

  const renderTracksList = (): JSX.Element => {
    return (
      <div className={styles.trackList}>
        {album &&
          album.tracks &&
          album.tracks.map((track, index) => renderTrack(track, index))}
      </div>
    );
  };

  const renderAlbumContainer = (): JSX.Element => {
    return (
      album && (
        <div className={styles.albumContainer}>
          <div className={styles.albumCover}>
            <img alt="logo" src={album.album_cover} className={styles.cover} />
            <div className={styles.downloadBtn}>
              {downloadCover ? (
                <Progress
                  type="circle"
                  percent={downloadCover?.progress}
                  className={`${styles.progressBar} ${
                    downloadCover.status === 'error' ? styles.error : ''
                  }`}
                  strokeWidth="10"
                  strokeColor={
                    downloadCover?.progress === 100 ? '#fff' : '#f39c12'
                  }
                  trailColor="#f6f5f"
                  width="20px"
                  showInfo={
                    downloadCover?.progress === 100 ||
                    downloadCover.status === 'error'
                  }
                  format={(percent) => {
                    if (downloadCover.status === 'error')
                      return <i className="fas fa-exclamation-circle" />;
                    return <i className="fas fa-check" />;
                  }}
                />
              ) : (
                <img src={coverIcon} alt="" />
              )}
              <span
                onClick={onDownloadCover}
                onKeyUp={onDownloadCover}
                role="button"
                tabIndex={0}
              >
                Album Cover
              </span>
            </div>

            <div className={styles.downloadBtn}>
              {downloadArtwork ? (
                <Progress
                  type="circle"
                  percent={downloadArtwork?.progress}
                  className={`${styles.progressBar} ${
                    downloadArtwork.status === 'error' ? styles.error : ''
                  }`}
                  strokeWidth="10"
                  strokeColor={
                    downloadArtwork?.progress === 100 ? '#fff' : '#f39c12'
                  }
                  trailColor="#f6f5f"
                  width="20px"
                  showInfo={
                    downloadArtwork?.progress === 100 ||
                    downloadArtwork.status === 'error'
                  }
                  format={(percent) => {
                    if (downloadArtwork.status === 'error')
                      return <i className="fas fa-exclamation-circle" />;
                    return <i className="fas fa-check" />;
                  }}
                />
              ) : (
                <img src={artworkIcon} alt="" />
              )}
              <span
                onClick={onDownloadArtwork}
                onKeyUp={onDownloadArtwork}
                role="button"
                tabIndex={0}
              >
                Booklet
              </span>
            </div>
          </div>

          <div className={styles.albumTracks}>
            <h4>{`SKU ${album.album_sku}`}</h4>
            <h2>{album.album_title}</h2>

            <h3>
              {album.artists &&
                album.artists.map((artist, index) => (
                  <span key={index}>
                    {artist}
                    {index === album.artists.length - 1 ? '' : ', '}
                  </span>
                ))}
            </h3>

            <div>
              <div className={styles.listHeader}>
                <div
                  onClick={toggleSelect}
                  onKeyUp={toggleSelect}
                  tabIndex={0}
                  role="button"
                  className={`${styles.deselectBtn} ${
                    !isChecked() ? styles.disabled : null
                  }`}
                >
                  {isChecked() && <span />}
                </div>
                <span>#</span>
                <span>Track name</span>
                <Button
                  className={styles.toggleSelectBtn}
                  onClick={toggleSelect}
                  disabled={albumTracks.length === 0}
                >
                  {isChecked() ? 'Deselect All' : 'Select All'}
                </Button>
              </div>

              <div className={styles.scrollArea}>{renderTracksList()}</div>
            </div>
          </div>
        </div>
      )
    );
  };

  const renderFooter = (): JSX.Element => {
    const tracks = getSelectedTracks();
    return (
      <div className={styles.footer}>
        <img src={background} alt="" className={styles.backgroundImg} />
        <h3>
          <b>{tracks}</b>
          {`Track${tracks > 1 ? 's' : ''} selected`}
        </h3>
        <div className={styles.selectWrapper}>
          <span>Quality</span>
          <select onChange={onQualityChange} defaultValue={soundFormat.id}>
            <option value="" disabled hidden>
              Select Quality
            </option>
            {album.sound_formats &&
              album.sound_formats.map((format, index) => {
                return (
                  <option
                    value={format.download_uuid}
                    key={format.download_format + index}
                  >
                    {format.download_format}
                  </option>
                );
              })}
          </select>
        </div>

        <button
          type="button"
          className={styles.downloadBtn}
          disabled={soundFormat.id === ''}
          onClick={onStartDownload}
        >
          Download now
          <i className="fas fa-arrow-down" />
        </button>
      </div>
    );
  };

  return (
    <div className={styles.container}>
      <div className={styles.backBtn}>
        <i
          className="fa fa-chevron-left fa-md"
          onClick={onOverview}
          onKeyUp={onOverview}
          role="button"
          label="link"
          tabIndex={0}
        />
        <span
          className={styles.titleBack}
          onClick={onOverview}
          onKeyUp={onOverview}
          role="button"
          tabIndex={0}
        >
          Back
        </span>
      </div>
      {renderAlbumContainer()}
      {getSelectedTracks() > 0 && showFooter && renderFooter()}
      {renderLoading()}

      {showPopup ? (
        <Popup props={props} type="logoutPopup" closePopup={togglePopup} />
      ) : null}
    </div>
  );
}
