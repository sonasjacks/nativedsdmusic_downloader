import React, { useContext, useEffect, useState } from 'react';

/** Contexts */
import { ErrorContext } from '../../contexts/ErrorContext';

/** Styles */
import styles from './Error.module.less';

export default function Error(props: any) {
  const { error, setError } = useContext(ErrorContext);
  const [propsError, setPropsError] = useState();

  const hideError = (): void => {
    setError(null);
  };

  const renderError = (): JSX.Element | null => {
    if (error) {
      setTimeout(hideError, 5000);
      return (
        <div className={styles.errorWrapper}>
          {error.message}
          <span
            onKeyUp={hideError}
            onClick={hideError}
            tabIndex={0}
            role="button"
          >
            <i className="fas fa-times" />
          </span>
        </div>
      );
    }
    return null;
  };
  return renderError();
}
