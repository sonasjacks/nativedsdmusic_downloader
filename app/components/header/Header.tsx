// @flow
import React, { useEffect, useState } from 'react';
import { Button, Layout } from 'antd';

/** Images */
import ImgLogo from '../../assets/logo.svg';

/** Styles */
import styles from './Header.module.less';

/** Utils */
import store from '../../utils/storage';

const { shell } = require('electron');

export default function Header(props: any): JSX.Element {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [firstName, setFirstName] = useState('');
  useEffect(() => {
    setLoggedIn(store.get('isLoggedIn', false));
    setFirstName(store.get('firstname', ''));
  }, []);

  const clickOnLogo = () => {
    props.history.push({
      pathname: '/',
    });
  };

  const onLogout = () => {
    props.onLogOut();
  };

  // const onDownloadPage = () => {
  //   props.history.push({
  //     pathname: '/downloadstatus'
  //   });
  // };

  const onSettings = () => {
    props.history.push({
      pathname: '/settings',
    });
  };

  const onHelp = () => {
    shell.openExternal(
      'http://help.nativedsd.com/en/collections/1826760-download-manager'
    );
  };
  return (
    <Layout.Header className={styles.header}>
      <Button type="text" onClick={clickOnLogo}>
        <strong>NativeCo.re</strong>
        Download Manager
      </Button>
      <div style={{ flexDirection: 'row', display: 'flex' }}>
        {isLoggedIn ? (
          <div
            style={{
              justifyContent: 'center',
              flexDirection: 'column',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <div
              style={{
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
                display: 'flex',
              }}
            >
              <span style={{ fontSize: '14px' }}>
                <strong>{firstName}</strong>
                is logged in -
              </span>
              <Button type="text" onClick={onLogout}>
                <i>Log out</i>
              </Button>
            </div>
            <div>
              <Button type="text" onClick={onSettings}>
                Settings
              </Button>
            </div>
          </div>
        ) : null}

        <img alt="logo" src={ImgLogo} className={styles.logo} />
      </div>
    </Layout.Header>
  );
}
