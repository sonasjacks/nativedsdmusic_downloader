export interface DownloadTrack {
  album_cover: string;
  album_sku: string;
  album_title: string;
  automatic: boolean;
  available: boolean;
  artists: string[];
  bytes: number;
  counter: number;
  progress: number;
  status: string;
  soundFormat: { id: string; name: string };
  track_number: string;
  track_title: string;
  track_length: string;
  track_preview: string;
  url: string;
}
