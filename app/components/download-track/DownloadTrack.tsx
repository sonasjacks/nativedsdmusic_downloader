/* eslint-disable prefer-destructuring */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-one-expression-per-line */
// @flow
import React, { useContext } from 'react';
import { Progress } from 'antd';
import moment from 'moment';

/** Assets */
import retryIcon from '../../assets/icons/retry-icon.svg';

/** Styles */
import styles from './DownloadTrack.module.less';

/** Utils */
import {
  cancelFileDownload,
  pauseFileDownload,
  resumeFileDownload,
  downloadFile,
  removeFileFromDownloadList,
  setErrorFileDownload,
  getCurrentDownloadingCount,
  getDownloadAlbums,
  setTrackUrl,
} from '../../utils/downloader';
import instance from '../../utils/apiConfig';
import { ErrorContext } from '../../contexts/ErrorContext';
import { DownloadTrack as Track } from './DownloadTrack.model';

export default function DownloadTrack(props: any): JSX.Element {
  const { track, layout } = props;
  const { error, setError } = useContext(ErrorContext);
  const cancelDownload = () => {
    if (track.status === 'error' || track.status === 'queued')
      removeFileFromDownloadList(track);
    else cancelFileDownload(track);
  };

  const pauseDownload = () => {
    pauseFileDownload(track);
  };

  const resumeDownload = () => {
    resumeFileDownload(track);
  };

  const getProgressStyle = (
    downloadTrack: Track
  ): {
    icon: JSX.Element;
    class: string;
    color: string;
    action: () => void;
  } => {
    switch (downloadTrack.status) {
      case 'progress':
        return {
          icon: <i className="fas fa-pause" />,
          class: styles.loading,
          color: '#f39c12',
          action: pauseDownload,
        };
        break;
      case 'paused':
        return {
          icon: <i className="fas fa-play" />,
          class: styles.paused,
          color: '#555',
          action: resumeDownload,
        };
        break;
      case 'queued':
        return {
          icon: <i className="far fa-clock" />,
          class: '',
          color: '#f6f5f2',
          action: null,
        };
        break;
      case 'finished':
        return {
          icon: <i className="fas fa-check" />,
          class: '',
          color: '#f6f5f2',
          action: null,
        };
      case 'error':
        return {
          icon: <i className="fas fa-exclamation-circle " />,
          class: styles.error,
          color: '#f6f5f2',
          action: null,
        };
      default:
        return {
          icon: <i className="far fa-clock" />,
          class: '',
          color: '#f39c12',
          action: null,
        };
    }
  };

  const onDownloadFinish = (): void => {
    if (getCurrentDownloadingCount() > 1) return;
    const tracks = getDownloadAlbums();

    const nextTrack = tracks.find((track) => track.status === 'queued');
    if (nextTrack) {
      downloadFile(nextTrack, onDownloadFinish).catch((err) => {
        setError(err);
        setErrorFileDownload(nextTrack);
        onDownloadFinish();
      });
    }
  };

  const onRetry = async () => {
    try {
      const response = await instance.post(
        `https://api.nativedsd.com/v2/downloads/${track.soundFormat.id}`,
        {
          track_numbers: [track.track_number],
        }
      );
      if (response.data.files[0]) {
        setTrackUrl(response.data.files[0]);
      } else {
        throw Error(
          `Cannot load tracks ${track.track_number} at this time. Please contact support.`
        );
      }
    } catch (err) {
      setError(err);
    }
    if (track.url) {
      downloadFile(track, onDownloadFinish).catch((err) => {
        setError(err);
        setErrorFileDownload(track);
        onDownloadFinish();
      });
    }
  };

  const transformDate = (date) => {
    const downloadDate = moment(date);
    const month = moment.monthsShort()[downloadDate.month()];
    const day = downloadDate.date();
    const hour = downloadDate.format('HH:mm');
    const isToday = downloadDate.isSameOrAfter(moment.now(), 'date');
    if (isToday) return `Today ${hour}`;

    return `${month}. ${day}, ${hour}`;
  };

  const getSize = (bytes) => {
    const size = track.bytes / 1000000;
    if (size > 1000) {
      return `${(size / 1000).toFixed(1)}  GB`;
    }
    return `${size.toFixed(1)}  MB`;
  };

  return (
    <div
      key={`${track.track_number}${track.album_sku}`}
      className={styles.track}
    >
      <div className={styles.progressWrapper}>
        <div
          onClick={getProgressStyle(track).action}
          onKeyDown={getProgressStyle(track).action}
          tabIndex="0"
          role="button"
          className={`${styles.status} ${getProgressStyle(track).class}`}
        >
          {getProgressStyle(track).icon}
        </div>
        <div className={`${styles.devider} ${getProgressStyle(track).class}`} />
        <div className={styles.progressContainer}>
          <div className={styles.progress}>{track.progress.toFixed()}%</div>
          <div className={styles.sizeDownloaded}>{getSize(track.bytes)}</div>
        </div>
      </div>
      {layout === 1 && (
        <h4 className={styles.trackInfo}>
          {`${track.album_title} - ${track.track_number}. ${track.track_title}`}
        </h4>
      )}
      {layout === 2 && (
        <h4 className={styles.trackInfo}>
          {`${track.track_number}   ${track.track_title}`}
        </h4>
      )}
      {(track.progress !== 100 || track.status === 'error') && (
        <div className={styles.actions}>
          {track.status === 'error' && (
            <button
              type="button"
              onClick={onRetry}
              className={styles.retryButton}
            >
              Retry
              <img src={retryIcon} alt="icon" />
            </button>
          )}
          <div
            className={styles.cancel}
            onClick={cancelDownload}
            onKeyUp={cancelDownload}
            role="button"
            tabIndex={0}
          >
            Cancel
            <i className="fas fa-times" />
          </div>
        </div>
      )}
      {track.status === 'finished' && layout === 1 && (
        <div className={styles.date}>{transformDate(track.downloadDate)}</div>
      )}
    </div>
  );
}
