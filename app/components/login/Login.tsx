/* eslint-disable react/jsx-one-expression-per-line */
import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import axios from 'axios';

/** Styles */
import styles from './Login.module.less';

/** Utils */
import store from '../../utils/storage';
import routes from '../../constants/routes.json';

/** Assets */
import logo from '../../assets/logo.svg';
import lockIcon from '../../assets/icons/lock-icon.svg';
import backgroundImage from '../../assets/login-background.svg';

const { shell } = require('electron');

export default function Login(props: any): JSX.Element {
  const [authToken, setAuthToken] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const { history } = props;

  useEffect((): any => {
    let mounted = true;
    const isLoggedIn = store.get('isLoggedIn', false);
    const url = 'http://api.nativedsd.com/v2/authorize/';

    if (isLoggedIn) {
      history.replace(routes.MY_CATALOG);
    }
    return () => {
      mounted = false;
    };
  }, []);

  const handleLogin = () => {
    const url = 'https://api.nativedsd.com/v2/authorize';
    axios
      .post(url, {
        auth_token: authToken,
      })
      .then((response) => {
        if (
          Object.prototype.hasOwnProperty.call(response.data, 'access_token')
        ) {
          const accessToken = response.data.access_token;
          const { user } = response.data;

          store.set('access_token', accessToken);
          store.set('isLoggedIn', true);
          store.set('firstname', user.first_name);
          store.set('lastname', user.last_name);

          axios.defaults.headers.common['X-ACCESS-TOKEN'] = accessToken;
          props.history.push({
            pathname: routes.MY_CATALOG,
            state: {
              action: 'showSettingsPopup',
            },
          });
          props.history.replace(routes.MY_CATALOG);
        } else if (
          Object.prototype.hasOwnProperty.call(response.data, 'message')
        ) {
          setErrorMessage(response.data.message);
        }
        return true;
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const setToken = (event): void => {
    setAuthToken(event.target.value);
  };
  const openLink = (): void => {
    shell.openExternal(
      'https://www.nativedsd.com/my-account/nativedsd-downloader/'
    );
  };
  return (
    <div className={styles.container}>
      <div className={styles.logo}>
        <img src={logo} alt="" />
        <img src={backgroundImage} alt="" className={styles.backgroundImg} />
      </div>
      <div className={styles.loginWrapper}>
        <h1>Authentication</h1>
        <p>
          An authentication token can be created{' '}
          <span
            onClick={openLink}
            onKeyUp={openLink}
            role="button"
            tabIndex={0}
          >
            in your account
          </span>
          . Log in if needed. Copy the generated token to your clipboard, then
          paste it below and click authorize.
        </p>
        <div className={styles.inputWrapper}>
          <input type="text" placeholder="Token value…" onBlur={setToken} />
          <button onClick={handleLogin} type="button">
            <span> Authorize</span>
            <img src={lockIcon} alt="" />
          </button>
        </div>
      </div>
    </div>
  );
}
