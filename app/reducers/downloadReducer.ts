// @fl

// MODEL

// / downloadAlbum
// / album, track info, status, progress, request, downloaded
// /
const INITIAL_STATE = {
  downloadAlbums: [],
  downloadCovers: [],
  downloadArtworks: [],
};

export default (state = INITIAL_STATE, action: any) => {
  let { downloadCovers, downloadAlbums, downloadArtworks } = state;
  let coverIndex;
  let artworkIndex;
  switch (action.type) {
    case 'SET_DOWNLOAD_ALBUMS':
      return { ...state, downloadAlbums: action.payload };
    case 'ADD_DOWNLOAD_ALBUMS':
      downloadAlbums = downloadAlbums.concat(action.payload);
      return { ...state, downloadAlbums };
    case 'SET_DOWNLOAD_COVERS':
      return { ...state, downloadCovers: action.payload };
    case 'SET_DOWNLOAD_ARTWORKS':
      return { ...state, downloadArtworks: action.payload };
    case 'ADD_DOWNLOAD_COVER':
      downloadCovers = downloadCovers.concat(action.payload);
      return { ...state, downloadCovers };

    case 'ADD_DOWNLOAD_ARTWORK':
      downloadArtworks = downloadArtworks.concat(action.payload);
      return { ...state, downloadArtworks };
    case 'REMOVE_DOWNLOAD_COVER':
      coverIndex = downloadCovers.findIndex((element: any) => {
        return element.album_sku === action.payload.album_sku;
      });
      downloadCovers.splice(coverIndex, 1);
      return { ...state, downloadCovers };
    case 'REMOVE_DOWNLOAD_ARTWORK':
      artworkIndex = downloadArtworks.findIndex((element: any) => {
        return element.album_sku === action.payload.album_sku;
      });
      downloadArtworks.splice(artworkIndex, 1);
      return { ...state, downloadArtworks };
    default:
      return state;
  }
};
