import React, { ReactNode, useState, useEffect } from 'react';
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { ErrorContext } from '../contexts/ErrorContext';
import Error from '../components/error/Error';

type Props = {
  children: ReactNode;
};

export default function App(props: Props) {
  const { children } = props;
  const [error, setError] = useState(null);

  const interceptResponse = (): void => {
    axios.interceptors.request.use((config: AxiosRequestConfig) => {
      const auth = {
        username: process.env.API_KEY,
        password: process.env.API_PASSWORD,
      };
      config.auth = auth;
      return config;
    });
    axios.interceptors.response.use(
      (response: AxiosResponse): AxiosResponse => {
        return response;
      },
      (err): Promise<void> => {
        const errorMessage = err?.response?.data || err;
        setError(errorMessage);
        return Promise.reject(errorMessage);
      }
    );
  };

  useEffect((): void => {
    interceptResponse();
  });

  return (
    <ErrorContext.Provider value={{ error, setError }}>
      {children}
      <Error />
    </ErrorContext.Provider>
  );
}
