import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions/downloads';
import DownloadList from '../components/download-list/DownloadList';

function mapStateToProps(state: any) {
  return {
    downloadAlbums: state.downloads.downloadAlbums,
  };
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DownloadList);
