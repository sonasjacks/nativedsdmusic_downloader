import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MyCatalog from '../components/my-catalog/MyCatalog';
import * as Actions from '../actions/downloads';

function mapStateToProps(state: any) {
  return {
    downloadAlbums: state.downloads.downloadAlbums,
  };
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MyCatalog);
