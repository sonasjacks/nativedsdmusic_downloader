import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Login from '../components/Login/Login';
// import * as Actions from '../actions';

function mapStateToProps(state: any) {
  return {
    downloadAlbums: {},
  };
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
