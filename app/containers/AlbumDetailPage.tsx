import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AlbumDetail from '../components/album-detail/AlbumDetail';
import * as Actions from '../actions/downloads';

function mapStateToProps(state: any) {
  return {
    downloadAlbums: state.downloads.downloadAlbums,
    downloadCovers: state.downloads.downloadCovers,
    downloadArtworks: state.downloads.downloadArtworks,
  };
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AlbumDetail);
