import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Settings from '../components/settings/Settings';
import * as Actions from '../actions/downloads';

function mapStateToProps(state: any) {
  return {
    downloadAlbums: state.downloads.downloadAlbums,
  };
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
