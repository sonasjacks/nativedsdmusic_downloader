/* eslint react/jsx-props-no-spreading: off */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import routes from './constants/routes.json';
import App from './containers/App';

/** Components */
import AlbumDetailPage from './containers/AlbumDetailPage';
import LoginPage from './containers/LoginPage';
import MyCatalogPage from './containers/MyCatalogPage';
import DownloadListPage from './containers/DownloadListPage';
import Error from './components/error/Error';

const LazySettingsPage = React.lazy(
  () =>
    import(/* webpackChunkName: "SettingsPage" */ './containers/SettingsPage')
);

const SettingsPage = (props: Record<string, any>) => (
  <React.Suspense fallback={<h1>Loading...</h1>}>
    <LazySettingsPage {...props} />
  </React.Suspense>
);

export default function Routes() {
  return (
    <App>
      <Switch>
        <Route exact path={routes.DOWNLOADLIST} component={DownloadListPage} />
        <Route path={routes.DETAIL} component={AlbumDetailPage} />
        <Route exact path={routes.LOGIN} component={LoginPage} />
        <Route exact path={routes.MY_CATALOG} component={MyCatalogPage} />
        <Route exact path={routes.SETTINGS} component={SettingsPage} />
      </Switch>
      <Error />
    </App>
  );
}
